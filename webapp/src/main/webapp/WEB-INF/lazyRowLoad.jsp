<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
<html>
<head>
  <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
  <script type="text/javascript"
          src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
  <script type="text/javascript"
          src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>


 <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"> </script>
 <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"> </script>
 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
 
 
 <script type="text/javascript" src="${pageContext.request.contextPath}/webapp/src/main/webapp/WEB-INF/js/autoComplete.js"></script>
 
</head>

<body>

<div class="container">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">
        <spring:message code="add.watchlist.title" />
      </h3>
    </div>
  
    <div style="margin-left: 400px;"><h5 style="color: red;"> ${error}</h5></div>
    <div class="panel-body">
      <form action="lazyRowAdd.web" class="form-horizontal" method="post" id="lazyList">
      	<div class="form-group">
      		<div class="col-sm-5">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		        <input type="hidden" name="acc_id" value="${theWatchListDesc.getWatchListDescKey().getAccountId()}"/>
		    </div>
	    </div>
        
        <div class="form-group">
          <label class="col-sm-4 control-label" for="watchListName"><strong><spring:message code="add.watchlist.label.watchListName" />
                            						</strong></label>
          <div class="col-sm-5">
            <input class="form-control" name="watchListName" id="watchListName" />
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-4 control-label" for="watchListDetails"><strong><spring:message code="add.watchlist.label.watchListDetails" />
                            						</strong></label>
          <div class="col-sm-5">
            <input class="form-control" name="watchListDetails" id="watchListDetails" />
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-4 control-label" for="marketDataFrequency"><strong><spring:message code="add.watchlist.label.marketDataFrequency" />
                            						</strong></label>
          <div class="col-sm-5">
            <input class="form-control" name="marketDataFrequency" id="marketDataFrequency" type="number" />
          </div>
        </div>
		<div class="form-group">
			 <label class="col-sm-4 control-label" for=""></label>
			<div class="col-sm-5" >
				<input type="button" value="Add Row" onclick="addRow('dataTable')"/>
	          	<input type="button" value="Delete Row" onclick="deleteRow('dataTable')"/>
	       	</div>
		</div>
          
        <div class="form-group">
          	<label class="col-sm-4 control-label" for="dataTable">
          			<strong><spring:message code="add.watchlist.label.market.data.symbol" />
                            						</strong></label>
            <table id="dataTable" border="1">
              <tr>
                <td><input type="checkbox" name="chk"/></td>
                <td>1</td>
                <td><input id="myText" onkeyup="ajaxcall(this)" autocomplete="off" name="operationParameterses[0].name" class="myInput"/></td>
              </tr>
            </table>
          </div>

		<div class="form-group">
          	<label class="col-sm-4 control-label" for=""></label>
          	<fieldset><button>Save User Details!</button></fieldset>
        </div>
      </form>
    </div>
  </div>

</div>


</body>
</html>

<script type="text/javascript">

    function addRow(tableID) {

        var table = document.getElementById(tableID);

        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);

        var cell1 = row.insertCell(0);
        var element1 = document.createElement("input");
        element1.type = "checkbox";
        element1.name = "chkbox[]";
        cell1.appendChild(element1);

        var cell2 = row.insertCell(1);
        cell2.innerHTML = rowCount + 1;
        var cell3 = row.insertCell(2);
        var element2 = document.createElement("input");
        element2.type = "text";
        var length = (table.rows.length) - 1;
        // element2.setAttribute("name","username1" );
         element2.setAttribute("id", "myText" + length);
         element2.setAttribute("type", "text");
         element2.setAttribute("onkeyup", "ajaxcall(this)");
        element2.name = "operationParameterses[" + length + "].name";
        element2.className = "myInput";
        element2.setAttribute("path", "name");
       
        cell3.appendChild(element2);

        $('.myInput').each(function () {
            $(this).rules("add",
                {
                    required: true,
                    minlength: 1,
                    email: true
                })
        });
    }

    function deleteRow(tableID) {
        try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;

            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if (null !== chkbox && true === chkbox.checked) {
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }
            }
        } catch (e) {
            alert(e);
        }
    }

    $(document).ready(function () {


        $('#lazyList').validate({

            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                username1: {
                    required: true,
                    minlength: 2
                },
                email1: {
                    required: true,
                    email: true
                }
            }
        });
        $('.myInput').each(function () {
            $(this).rules("add",
                {
                    required: true,
                    minlength: 2
                })
        });
        $(".table").rules("add", {
            required: true,
            minlength: 3
        });
    });
</script>
 <script>
			//$(function() {
 function ajaxcall(elem){
	 var id = ($(elem).attr('id'));
	// alert(id);
				 var searchAjax = function(request, response) {
					var data = {}
					data["searchParam"] = request.term;
					$.ajax({
						type : "GET",
						contentType : "application/json;charset=utf-8",
						url : "${home}/webapp/admin/ajax_search2?",
						data : data,
						dataType : 'json',
						processData : true,
						timeout : 100000,
						success : function(ajaxresponse) {
							console.log("SUCCESS: ", ajaxresponse);
							var instlist = ajaxresponse.item;
							response(instlist);
							console.log(instlist);
						},
						error : function(e) {
							console.log("ERROR--: ", e);
						},
						done : function(e) {
							console.log("DONE");
						}
					});
				};

				var selectItem = function(event, ui) {
					$("#" + id).val(ui.item.value);
					return false;
				}

				$("#"+id).autocomplete({
									source : searchAjax,
									select : selectItem,
									minLength : 1,
									change : function() {
										$("#"+id).val(document.getElementById("#"+id).value);
									}
								}); 
 }
			//});
		</script>
