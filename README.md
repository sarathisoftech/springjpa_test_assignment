**Spring JPA Test Assignment**

Need more 8 hours to complete the task of importing and exporting xml for account data.
So the Estimated completion date is Monday 27th, 2017.

Updated the Steps to Deploy.

Added two profile for test and development environments.
Test will initialize the database on every startup of server.
Development will only initialize the database at very first time.
Default profile is development.

Test cases completed!


Steps to Deploy the Application
---

*1*. Import project in to IDE.

*2*. Change database properties such as **user-name, password, driver, dialect or other properties** in following two files:
		 **'/config/src/main/resources/liquibase.properties'**
	and  **'/dao/src/test/resources/jdbcTemp.properties'**.
		**liquibase.properties**: this database is used as production database for the populating database using liquibase
		**jdbcTemp.properties**: this database is used for Junit test cases.

*3*. Create database-schema named as **mentioned in above properties files**.

*4*. Right click on main module: '/springjpa_test_assignment' and then execute 'Maven clean' command.

*5*. Rignt click on main module: '/springjpa_test_assignment' and then execute 'Maven install' command.

*6*. Again Right click on main module: '/springjpa_test_assignment' and then click on Maven -> Update Project -> Select All Modules -> select 'Force Update of Snapshots' -> OK

*7*. Add tomcat user role in tomcat-users.xml as:
        **(Note : This configuration is required to deploy the Application into Tomcat server using tomcat-maven plugins)**

		<role rolename="manager-gui"/>
  		<user password="s3cret" roles="manager-gui" username="tomcat"/>
	
*8*. Update above username and password in '/springjpa_test_assignment/pom.xml'.

			<plugin>
				<groupId>org.apache.tomcat.maven</groupId>
				<artifactId>tomcat7-maven-plugin</artifactId>
				<version>2.2</version>
				<configuration>
					<url>http://localhost:8080/manager/html</url>
					<server>TomcatServer</server>
					<path>/webapp</path>
					<username>tomcat</username>
					<password>s3cret</password>
				</configuration>
			</plugin>

*9*. Open terminal and navigate to project folder 'springjpa_test_assignment'. Execute the 

		command as: **'mvn tomcat7:run-war'** --default profile
		
		for test: **'mvn tomcat7:run-war -Ptest'**
		
		for development: **'mvn tomcat7:run-war -Pdev -Dmaven.test.skip=true'**
		
*10*. Application started at the url: **'http://localhost:8080/webapp/'**

*11*. Default user-name:**'FinBackTesting' and password:'123qwe'**