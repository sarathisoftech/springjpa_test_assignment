package com.gjj.igden.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {
	
	private static final Logger logger = LoggerFactory.getLogger(GreetingController.class);
	
	@GetMapping({"/", "/welcome**"})
  public ModelAndView defaultPage() {
	  logger.debug("Default page");
    ModelAndView model = new ModelAndView();
    model.addObject("title", "Spring Security Remember Me");
    model.addObject("message", "This is default page!");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth.getPrincipal().equals("anonymousUser")) {
      model.setViewName("login");
    } else {
      model.setViewName("redirect:" + "/admin/list-accounts");
    }
    return model;
  }

	@GetMapping("/admin/search-auth")
  public String searchPage1() {
	  logger.debug("Redirecting to /admin/search-auth");
    return "search-auth";
  }

	@GetMapping("/admin**")
  public ModelAndView adminPage() {
	  logger.debug("Admin page");
    ModelAndView model = new ModelAndView();
    model.addObject("title", "Spring Security Remember Me");
    model.addObject("message", "This page is for ROLE_ADMIN only!");
    model.setViewName("admin");
    return model;
  }

	@GetMapping("/logout**")
  public String logOut() {
	  logger.debug("Logged out");
    return "redirect:login";
  }

  @GetMapping(value = "/ajax-stock")
  public String ajaxStock() {
	  logger.debug("Redirecting to ajax-stock");
    return "ajax-stock";
  }

  @GetMapping("/admin/update**")
  public ModelAndView updatePage(HttpServletRequest request) {
	  logger.debug("Checking remember me is authenticated or not");
    ModelAndView model = new ModelAndView();
    if (isRememberMeAuthenticated()) {
      //send login for update
      setRememberMeTargetUrlToSession(request);
      model.addObject("loginUpdate", true);
      model.setViewName("login");
    } else {
      model.setViewName("update");
    }
    return model;
  }

  @GetMapping("/login")
  public ModelAndView login(@RequestParam(value = "error", required = false) String error,
	  @RequestParam(value = "logout", required = false) String logout,
        HttpServletRequest request) {
	  
		ModelAndView model = new ModelAndView();
		if (error != null) {
			logger.debug("Login page error" + error);
			model.addObject("error", "Invalid username and password!");

			//login form for update page
                      //if login error, get the targetUrl from session again.
			String targetUrl = getRememberMeTargetUrlFromSession(request);
			if(StringUtils.hasText(targetUrl)){
				logger.debug("Login page target url" + targetUrl);
				model.addObject("targetUrl", targetUrl);
				model.addObject("loginUpdate", true);
			}
		}

		if (logout != null) {
			logger.debug("Logged out successfully" + logout);
			model.addObject("msg", "You've been logged out successfully.");
		}
		
		model.setViewName("login");

		return model;

  }
  
  private boolean isRememberMeAuthenticated() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return authentication != null && RememberMeAuthenticationToken.
      class.isAssignableFrom(authentication.getClass());
  }

  private void setRememberMeTargetUrlToSession(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session != null) {
      session.setAttribute("targetUrl", "/admin/update");
    }
  }

  private String getRememberMeTargetUrlFromSession(HttpServletRequest request) {
    String targetUrl = "/admin/list-accounts";
    HttpSession session = request.getSession(false);
    if (session != null) {
      targetUrl = session.getAttribute("targetUrl") == null
        ? "/admin/list-accounts" : session.getAttribute("targetUrl").toString();
    }
    return targetUrl;
  }
  
}