package com.gjj.igden.model;

public class OperationParameters {

	private Long id;

	private String name;

	public OperationParameters(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public OperationParameters() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}